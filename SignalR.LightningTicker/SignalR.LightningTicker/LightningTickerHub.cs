﻿using System.Collections.Generic;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using SignalR.LightningTicker.Domain;
using System.Threading.Tasks;

namespace SignalR.LightningTicker
{
    [HubName("lightningTickerHub")]
    public class LightningTickerHub : Hub
    {
        private readonly LightningTicker _lightningTicker;

        public LightningTickerHub() : this(LightningTicker.Instance) { }

        public LightningTickerHub(LightningTicker stockTicker)
        {
            _lightningTicker = stockTicker;
        }


        public IEnumerable<LightningRestData> GetLastHour()
        {
            return _lightningTicker.GetLastHour();
        }
        public IEnumerable<LightningRestData> GetLastHalfHour()
        {
            return _lightningTicker.GetLastHalfHour();
        }
        public IEnumerable<LightningRestData> GetLastFiveteenMin()
        {
            return _lightningTicker.GetLastFiveteenMin();
        }
        public IEnumerable<LightningRestData> GetLastFiveMin()
        {
            return _lightningTicker.GetLastFiveMin();
        }
        public IEnumerable<LightningRestData> GetHourFrom(string dateString)
        {
            return _lightningTicker.GetHourFrom(dateString);
        }

    }
}