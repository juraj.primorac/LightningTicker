﻿using System;
using System.IO;
using System.Net;
using System.Text;
using System.Web.Script.Serialization;

public enum HttpVerb
{
    GET,
    POST,
    PUT,
    DELETE
}



namespace SignalR.LightningTicker.Domain
{
    public class RestClient
    {
        //test
        public static int i = 0;
        public static string path = @"c:\Users\jprim\Desktop\text";


        public string EndPoint { get; set; }
        public HttpVerb Method { get; set; }
        public string ContentType { get; set; }
        public string PostData { get; set; }

        public RestClient()
        {
            EndPoint = "";
            Method = HttpVerb.GET;
            ContentType = "text/xml";
            PostData = "";
        }
        public RestClient(string endpoint)
        {
            EndPoint = endpoint;
            Method = HttpVerb.GET;
            ContentType = "text/xml";
            PostData = "";
        }
        public RestClient(string endpoint, HttpVerb method)
        {
            EndPoint = endpoint;
            Method = method;
            ContentType = "text/xml";
            PostData = "";
        }

        public RestClient(string endpoint, HttpVerb method, string postData)
        {
            EndPoint = endpoint;
            Method = method;
            ContentType = "text/xml";
            PostData = postData;
        }


        public string MakeRequest()
        {
            return MakeRequest("");
        }

        public string MakeRequest(string parameters)
        {
            var request = (HttpWebRequest)WebRequest.Create(EndPoint + parameters);

            request.Method = Method.ToString();
            request.ContentLength = 0;
            request.ContentType = ContentType;

            if (!string.IsNullOrEmpty(PostData) && Method == HttpVerb.POST)
            {
                var encoding = new UTF8Encoding();
                var bytes = Encoding.GetEncoding("iso-8859-1").GetBytes(PostData);
                request.ContentLength = bytes.Length;

                using (var writeStream = request.GetRequestStream())
                {
                    writeStream.Write(bytes, 0, bytes.Length);
                }
            }

            
                using (var response = (HttpWebResponse)request.GetResponse())
                {
                    var responseValue = string.Empty;

                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        var message = String.Format("Request failed. Received HTTP {0}", response.StatusCode);
                        throw new ApplicationException(message);
                    }

                    // grab the response
                    using (var responseStream = response.GetResponseStream())
                    {
                        if (responseStream != null)
                            using (var reader = new StreamReader(responseStream))
                            {
                                responseValue = reader.ReadToEnd();
                            }
                    }

                    return responseValue;
                }

        }

        public RestOdg getRecent(Int64 ticks)
        {
            this.EndPoint = "http://161.53.66.173/rest/Service1.svc/GRT";
            this.Method = HttpVerb.GET;
            Int64 timeTicks = DateTime.UtcNow.Ticks-ticks;
            DateTime time = new DateTime(timeTicks);
            string timeString=time.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fff'Z'");
            var json = this.MakeRequest("?t=" + timeString);
            //var json=this.getTestJson();
            JavaScriptSerializer oJS = new JavaScriptSerializer();
            oJS.MaxJsonLength = Int32.MaxValue;
            RestOdg restOdg = new RestOdg();
            restOdg = oJS.Deserialize<RestOdg>(json);

            

            return restOdg;
        }

        public RestOdg getNewLightnings(Int64 lastUpdate)
        {
            this.EndPoint = "http://161.53.66.173/rest/Service1.svc/GRT";
            this.Method = HttpVerb.GET;
            Int64 timeTicks = lastUpdate;
            DateTime time = new DateTime(timeTicks);
            string timeString =time.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fff'Z'");
            var json = this.MakeRequest("?t=" + timeString);
            //var json = this.getTestJson();
            JavaScriptSerializer oJS = new JavaScriptSerializer();
            oJS.MaxJsonLength = Int32.MaxValue;
            RestOdg restOdg = new RestOdg();
            restOdg = oJS.Deserialize<RestOdg>(json);


            return restOdg;
        }

        public RestOdg getHourFrom(string from)
        {
            this.EndPoint = "http://161.53.66.173/rest/Service1.svc/GetH";
            this.Method = HttpVerb.GET;
            DateTime myLocalDate = DateTime.ParseExact(from+":00", "yyyy-MM-dd'T'HH:mm:ss",System.Globalization.CultureInfo.InvariantCulture);
            DateTime myUtcDate = TimeZoneInfo.ConvertTimeToUtc(myLocalDate, TimeZoneInfo.Local);
            var json="";
            string timeString = myUtcDate.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'Z'");
            Int64 diff = DateTime.UtcNow.Ticks - myUtcDate.Ticks;

            if (diff<= TimeSpan.TicksPerHour)
            {
                if (diff<=TimeSpan.TicksPerSecond)
                {
                    return null;
                }

                this.EndPoint = "http://161.53.66.173/rest/Service1.svc/GRT";
                json = this.MakeRequest("?t=" + timeString);
            }
            else
            {
                json = this.MakeRequest("?t=" + timeString + "&o=3600");
            }           
            //var json = this.getTestJson();
            JavaScriptSerializer oJS = new JavaScriptSerializer();
            oJS.MaxJsonLength = Int32.MaxValue;
            RestOdg restOdg = new RestOdg();
            restOdg = oJS.Deserialize<RestOdg>(json);


            return restOdg;
        }


        public string getTestJson()
        {
            
            string myPath;

            if (RestClient.i > 5) RestClient.i = 5;


            if (RestClient.i==0)
            {
                myPath = RestClient.path+ ".txt";
                ++RestClient.i;
            }
            else
            {
                myPath =RestClient.path+i+".txt";
                ++RestClient.i;
            }
            
            return File.ReadAllText(myPath);

        }

    } // class
}