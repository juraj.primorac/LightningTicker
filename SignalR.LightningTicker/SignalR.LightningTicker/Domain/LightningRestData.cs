﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SignalR.LightningTicker.Domain
{
    public class LightningRestData
    {
        public Int32 Id { get; set; }

        public Int64 Timestamp { get; set; }

        public Single Longitude { get; set; }

        public Single Latitude { get; set; }

        public Int32 Altitude { get; set; }

        //public StrokeKind Type { get; set; }

        public Single Current { get; set; }

        public Int32 Error { get; set; }


        public string Time { get; set; }

        public Int32 Life { get; set; }

    }
}