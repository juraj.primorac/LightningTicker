﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;


using System.Web.Script.Serialization;
using SignalR.LightningTicker.Domain;
using System.Linq;


namespace SignalR.LightningTicker
{
    public class LightningTicker
    {
        // Singleton instance
        private static Lazy<LightningTicker> _instance = new Lazy<LightningTicker>(() => new LightningTicker(GlobalHost.ConnectionManager.GetHubContext<LightningTickerHub>().Clients));


        //was readonly
        private ConcurrentBag<LightningRestData> _lightnings =  new ConcurrentBag<LightningRestData>();

        private readonly object _addNewLightningsLock = new object();



        

        //chage this to 20s
        private readonly TimeSpan _updateInterval = TimeSpan.FromMilliseconds(10000);
        private readonly Timer _timer;

        private volatile bool _addingNewLightnings = false;

        private Int64 lastUpdate;



        private LightningTicker(IHubConnectionContext<dynamic> clients)
        {
            Clients = clients;

            //_lightnings = new ConcurrentBag<LightningRestData>();



            // _lightnings.Clear();


            this.restClient = new RestClient();
            RestOdg restOdg= restClient.getRecent(TimeSpan.TicksPerHour);
            this.lastUpdate = restOdg.lu;
            var hourOfLightning = new List<LightningRestData>(restOdg.data);

           
            hourOfLightning.ForEach(lightning => {               
                lightning.Time = (new DateTime(lightning.Timestamp).ToLocalTime()).ToString("dd'-'MM'-'yyyy';'HH':'mm':'ss'.'fff");
                lightning.Life = (Int32)Math.Ceiling(((DateTime.UtcNow.Ticks - new DateTime(lightning.Timestamp).Ticks) * 0.0000001));
                _lightnings.Add(lightning);
               
            });
            _timer = new Timer(addNewLightnings, null, _updateInterval, _updateInterval);

            
        }

        public static LightningTicker Instance
        {
            get
            {
                return _instance.Value;
            }
        }

        private IHubConnectionContext<dynamic> Clients
        {

            get;
            set;
        }

        private RestClient restClient
        {
            get;
            set;
        }

        public IEnumerable<LightningRestData> GetLastHour()
        {

            List<LightningRestData> filtered = null;

            lock (_addNewLightningsLock)
            {
                if (!_addingNewLightnings)
                {
                    _addingNewLightnings = true;
                    filtered=new List<LightningRestData>( _lightnings);
                    _addingNewLightnings = false;
                }
            }
            filtered.ForEach(l=> l.Life = (Int32)Math.Ceiling(((DateTime.UtcNow.Ticks - new DateTime(l.Timestamp).Ticks) * 0.0000001)));
            return filtered;

        }

        public IEnumerable<LightningRestData> GetLastHalfHour()
        {
            Int64 timeBefore30Min= DateTime.UtcNow.Ticks - TimeSpan.TicksPerHour/2;

            List<LightningRestData> filtered = null;

            lock (_addNewLightningsLock)
            {
                if (!_addingNewLightnings)
                {
                    _addingNewLightnings = true;
                    filtered = new List<LightningRestData>(_lightnings.Where(l => l.Timestamp >= timeBefore30Min));

                    _addingNewLightnings = false;
                }
            }
            filtered.ForEach(l => l.Life = (Int32)Math.Ceiling(((DateTime.UtcNow.Ticks - new DateTime(l.Timestamp).Ticks) * 0.0000001)));
            return filtered;


        }

        public IEnumerable<LightningRestData> GetLastFiveteenMin()
        {
            Int64 timeBefore15Min = DateTime.UtcNow.Ticks - TimeSpan.TicksPerMinute*15;
            List<LightningRestData> filtered = null;

            lock (_addNewLightningsLock)
            {
                if (!_addingNewLightnings)
                {
                    _addingNewLightnings = true;
                    filtered = new List<LightningRestData>(_lightnings.Where(l => l.Timestamp >= timeBefore15Min));
                    _addingNewLightnings = false;
                }
            }
            filtered.ForEach(l => l.Life = (Int32)Math.Ceiling(((DateTime.UtcNow.Ticks - new DateTime(l.Timestamp).Ticks) * 0.0000001)));
            return filtered;

        }

        public IEnumerable<LightningRestData> GetLastFiveMin()
        {
            Int64 timeBefore5Min = DateTime.UtcNow.Ticks - TimeSpan.TicksPerMinute * 5;
            List<LightningRestData> filtered=null;

            lock (_addNewLightningsLock)
            {
                if (!_addingNewLightnings)
                {
                    _addingNewLightnings = true;

                    filtered = new List<LightningRestData>( _lightnings.Where(l=>l.Timestamp>=timeBefore5Min));

                    _addingNewLightnings = false;
                }
            }
            filtered.ForEach(l => l.Life = (Int32)Math.Ceiling(((DateTime.UtcNow.Ticks - new DateTime(l.Timestamp).Ticks) * 0.0000001)));
            return filtered;
        }

        public IEnumerable<LightningRestData> GetHourFrom(string dateString)
        {
            string myDateString = dateString.Trim();
            List<LightningRestData> filtered = new List<LightningRestData>();
            RestOdg restOdg = restClient.getHourFrom(dateString);
            if (restOdg == null) return filtered;
            filtered = new List<LightningRestData>(restOdg.data);
            return filtered;
        }





        private void addNewLightnings(object state)
        {
            lock (_addNewLightningsLock)
            {
                if (!_addingNewLightnings)
                {
                    _addingNewLightnings = true;

                    
                    Int64 x = DateTime.UtcNow.Ticks - TimeSpan.TicksPerHour;
                     _lightnings = new ConcurrentBag<LightningRestData>(_lightnings.Where(l => l.Timestamp >= x));  
                       // _lightnings.Add(_lightnings.Where(l => l.Timestamp >= x))
                    RestOdg restOdg= restClient.getNewLightnings(lastUpdate);
                    this.lastUpdate = restOdg.lu;
                    List<LightningRestData> newLightnings;
                    newLightnings = new List<LightningRestData>(restOdg.data);
                    if (restOdg.data.Length > 0)
                    {
                        newLightnings.OrderBy(l => l.Timestamp);
                        newLightnings.ForEach(lightning =>
                        {                            
                            lightning.Time = (new DateTime(lightning.Timestamp).ToLocalTime()).ToString("dd'-'MM'-'yyyy';'HH':'mm':'ss'.'fff");
                            lightning.Life = (Int32)Math.Ceiling(((DateTime.UtcNow.Ticks - new DateTime(lightning.Timestamp).Ticks) * 0.0000001));
                            _lightnings.Add(lightning);

                        });
                    }


                    this.BroadcastNewLightnings(newLightnings);
                    



                    _addingNewLightnings = false;
                }
            }
        }



        private void BroadcastNewLightnings(IEnumerable<LightningRestData> newLightnings)
        {
            
           Clients.All.updateLightningList(newLightnings);
        }


        
    }
}