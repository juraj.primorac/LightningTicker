#  Lightnings locator system user interface

Front-end and back-end support for an application that informs its users in real time about the current lightning activity, as well as the aggregate lightning activity in time and space. In our well designed system each component has a unique and a well defined role. The functionality is implemented in a distributed system consisting of three main components: a REST server(not in this project) hosting the raw data about the time and location of lightnings, an ASP.NET(SignalR framework) server in charge of preprocessing the data, and finally a webpage that presents the final results to the user. 


## Prerequisites

Visual Studio supporting ASP.NET SignalR framework, browser supportting SignalR(https://docs.microsoft.com/en-us/aspnet/signalr/overview/getting-started/supported-platforms), internet connection

## Running

Open solution with Visual Studio and run with selected browser.

## Built With

* [SignalR](https://www.google.hr/url?sa=t&rct=j&q=&esrc=s&source=web&cd=1&cad=rja&uact=8&ved=0ahUKEwi84vOm-f_XAhVRzKQKHUnYA7oQFggoMAA&url=https%3A%2F%2Fwww.nuget.org%2Fpackages%2FMicrosoft.AspNet.SignalR%2F2.1.2&usg=AOvVaw3ZYwx31ir8zTfC72pPsblj) - The ASP.NET framework used
* [jQuery](https://www.nuget.org/packages/jQuery/1.10.2/) - JavaScript library
* [MarkerClusterer](https://www.google.hr/url?sa=t&rct=j&q=&esrc=s&source=web&cd=1&cad=rja&uact=8&ved=0ahUKEwjTq5X17P_XAhVQFOwKHRyiAKIQFggoMAA&url=https%3A%2F%2Fgithub.com%2Fgooglemaps%2Fjs-marker-clusterer&usg=AOvVaw0UaTH8ofCOZLCm5HwYM47H) - JavaScript library used for dispaying markers
* [Bootstrap](http://blog.getbootstrap.com/2016/07/25/bootstrap-3-3-7-released/) - HTML, CSS, and JS framework
* [GoogleMapsApi](https://developers.google.com/maps/documentation/javascript/get-api-key) - Google Maps API



## Author

* **Juraj Primorac** 


